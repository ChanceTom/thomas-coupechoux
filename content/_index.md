### Current situation
I'm a **IT system engineer** at Gricad (Grenoble Alpes Recherche - Infrastructure de Calcul Intensif et de Données). I'm have to upgrade Perseus portal. It's a web portal to access to the data center.  

If you want to know more about my curriculum, go to [Career](/page/career).


### Contact
COUPECHOUX Thomas  
thomas.coupechoux@univ-grenoble-alpes.fr  
Gricad - Imag Building  
700 avenue Centrale  
Office: 256