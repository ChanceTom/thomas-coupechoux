---
title: My career
comments: false
---
## Experience
### February 2019
**IT system engineer** at GRICAD

I've to update the web portal PERSEUS. It's a portal that allows you to post a calculus project. If it's accepted by members of you scientific poles, it allows you to connect to the Grenoble Data Center.  
**Key words:** Python, VueJS, LDAP


### April 2018 - September 2017
**Internship Research and Development** at LAMA, Savoie's University

I had to implement in C++ a new term in the solution of a equation. This term will allow to obtain the color's continuity in the regularization of a voxel model. I did also some optimization of the code with some parallelization.  
If you want to see the powerpoint, it's available [here](https://gricad-gitlab.univ-grenoble-alpes.fr/coupecht/thomas-coupechoux/blob/master/publication/Presentation-M2-info.pdf). And the master thesis [here](https://gricad-gitlab.univ-grenoble-alpes.fr/coupecht/thomas-coupechoux/blob/master/publication/Rapport_Thomas_COUPECHOUX-m2-info.pdf).  
**Key words:** C++, QT, OpenMP, Linux, Maths
### Mars 2018 - October 2017
**Profesionnal Project** at LAMA

With a team of 6 people, we had to studied and implemented a new algorithm that simulate fire spreading. I've help the other to study the algoritm and popularized the mathematicals notions.  
**Key words:** C++, Linux, Maths

### August 2017 - Jully 2017
**Cashier** at the Boite à outils

### March 2017 - Jully 2017
**Internship Research and Development** at LAMA, Savoie's University
I've studied the problem of Ciorascu and Murat. I've also implemented in C++ an algorithm of image compression and decompression using diffusion equations. We choose "important" points, choosen by the mathematical theory. With theses points, we were able to reconstruct the approximate image. In order to have a good idea of the proportions, we took 5% of the inital image.  
If you want to see the powerpoint, it's available [here](https://gricad-gitlab.univ-grenoble-alpes.fr/coupecht/thomas-coupechoux/blob/master/publication/COUPECHOUX_Thomas_diapoM2.pdf). And the master thesis [here](https://gricad-gitlab.univ-grenoble-alpes.fr/coupecht/thomas-coupechoux/blob/master/publication/COUPECHOUX_Thomas-rapportM2.pdf).  
**Key words:** C++, Maths, OpenCV, Matlab, OpenMP, Inverse Problem

### March 2016 - Jully 2016
**Internship Research and Development** at LAMA, Savoie's University
I've studied a new algorithm of image compression and decompression. I implemented a POC in matlab. This works has been upgraded the next internship.  
If you want to see the powerpoint, it's available [here](https://gricad-gitlab.univ-grenoble-alpes.fr/coupecht/thomas-coupechoux/blob/master/publication/soutenanceM1.pdf). And the master thesis [here](https://gricad-gitlab.univ-grenoble-alpes.fr/coupecht/thomas-coupechoux/blob/master/publication/RapportM1.pdf).  
**Key words:** Maths, Matlab, Inverse Problem, Equations

## Educations
### Master degree second year
IT master degree  
**Key Words:** C++, JEE, Distributed Algorithm, Cryptology, Security

### Master degree 
Math master degree  
**Key Words:** C++, Cryptology, Diffusion Equations, Geometry, Optimal Transport equations, Actifs contours, ...

### Licence degree
Math licence degree  
**Key Words:** Matlab, Linear Algebra, Algebra, Symbolic Calculus, Differential equations, Solving methods
