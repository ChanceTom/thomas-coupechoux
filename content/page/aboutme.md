---
title: About me
subtitle: Who am I
comments: false
---

My name is Thomas. I have the following qualities:

- I can adapt myself to many situations
- I love games in differents ways (videos games, boards games, escape games)
- I like photos

I'm interested in all scientific content. I want to learn how the world is working because it's beautiful.